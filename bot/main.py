import os
import jokes
import telebot
import random

from telebot import types

bot = telebot.TeleBot(os.environ['TG_API_TOKEN'])


@bot.message_handler(commands=["start"])
def hello_message(message):
    bot.send_message(message.from_user.id, "Как тебя зовут?")
    bot.register_next_step_handler(message, get_name)

def get_name(message):
    global name
    name = message.text

    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    item1 = types.KeyboardButton("/joke")
    item2 = types.KeyboardButton("/meme")
    item3 = types.KeyboardButton("/help")
    markup.add(item1, item2, item3)

    bot.send_message(message.from_user.id, "Привет, "+name+"!", reply_markup=markup)



@bot.message_handler(content_types=["text"])
def answer(message): 
    
    if message.text == '/joke':

        markup = types.InlineKeyboardMarkup(row_width=3)
        item1 = types.InlineKeyboardButton("1", callback_data='1')
        item2 = types.InlineKeyboardButton("2", callback_data='2')
        item3 = types.InlineKeyboardButton("3", callback_data='3')
        item4 = types.InlineKeyboardButton("4", callback_data='4')
        item5 = types.InlineKeyboardButton("5", callback_data='5')
        item6 = types.InlineKeyboardButton("6", callback_data='6')

        markup.add(item1, item2, item3, item4, item5, item6)

        bot.send_message(message.from_user.id, "Выбери шутку:", reply_markup=markup)

    elif message.text == '/meme':

        markup = types.InlineKeyboardMarkup(row_width=3)
        item1 = types.InlineKeyboardButton("1", callback_data='101')
        item2 = types.InlineKeyboardButton("2", callback_data='102')
        item3 = types.InlineKeyboardButton("3", callback_data='103')
        item4 = types.InlineKeyboardButton("4", callback_data='104')
        item5 = types.InlineKeyboardButton("5", callback_data='105')
        item6 = types.InlineKeyboardButton("6", callback_data='106')
 
        markup.add(item1, item2, item3, item4, item5, item6)

        bot.send_message(message.from_user.id, "Выбери мем:", reply_markup=markup)

    elif message.text == '/help':
        bot.send_message(message.from_user.id, "Тебе действительно нужна помощь с двумя кнопками?\nСпециально для тебя объясняю:\nУмею шутки шутить /joke\nА если просто букавы для тебя слишком сложно, могу картинку отправить /meme")
    else: 
        bot.send_message(message.chat.id, "На кнопки тыкай. /help")

@bot.callback_query_handler(func=lambda call: True)
def callback_inline(call):
    try:
        if call.message:
            if int(call.data) in range(1,7):
                bot.send_message(call.message.chat.id, eval('jokes.joke'+call.data))
            elif int(call.data) in range(101,107):
                picture = "pictures/"+call.data+".jpg"
                bot.send_photo(call.message.chat.id, open(picture, 'rb'))
            else:
                bot.send_message(call.message.chat.id, "Что-то пошло не так.") 
 
    except Exception as e:
        print(repr(e))



if __name__ == '__main__':
     bot.infinity_polling()
