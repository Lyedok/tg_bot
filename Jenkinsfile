#!groovy

properties([disableConcurrentBuilds()])


pipeline {
	agent any
	options {
		buildDiscarder(logRotator(numToKeepStr: '5', artifactNumToKeepStr: '5'))
		timestamps()
	}
	stages {
		stage("Build") {
			steps {
				echo "========== Building image =========="
				sh 'docker build -t lyedok/tg_bot .'
			}
		}
		stage("Push") {
			steps {
				echo "========== Pushing to Docker Hub =========="
				withCredentials([usernamePassword(credentialsId: 'dockerhub_lyedok', usernameVariable: 'dockerhub_username', passwordVariable: 'dockerhub_password')]) {
					sh """
					docker login -u $dockerhub_username -p $dockerhub_password
					docker push lyedok/tg_bot
					docker logout
					"""
				}
			}
		}
		stage("Deploy") {
			steps {
				echo "========== Deploying to server =========="
				withCredentials([
					sshUserPrivateKey(credentialsId: 'aws_server', keyFileVariable: 'SSH_KEY', usernameVariable: 'SSH_USER'),
					string(credentialsId: 'aws_host', variable: 'SSH_HOST'),
					string(credentialsId: 'tg_token', variable: 'TG_TOKEN')
				]) {
					sh """
					ssh -o StrictHostKeyChecking=no $SSH_USER@$SSH_HOST -i $SSH_KEY '''
					docker pull lyedok/tg_bot &&\
					if [ \$(docker ps -aqf 'name=tg_bot') ]; then docker kill \$(docker ps -aqf 'name=tg_bot') &&\
					docker rm \$(docker ps -aqf 'name=tg_bot'); \
					else echo 'Old container not found'; fi &&\
					docker run -d -e TG_API_TOKEN=$TG_TOKEN --name tg_bot lyedok/tg_bot
					'''
					"""
				}

			}
		}
	}
}