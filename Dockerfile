FROM python:3.9.10-alpine3.15

ENV TZ=Europe/Kaliningrad
ENV TG_API_TOKEN=''

RUN apk add --no-cache tzdata && pip install pytelegrambotapi

WORKDIR /app

COPY ./bot .

CMD ["python", "-u", "main.py"]
